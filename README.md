# Re-Make

Development is currently inactive. Many files of this project can be found in the [RVGL Core Assets](https://gitlab.com/re-volt/rvgl_core_assets) projects which has different goals but supports the idea of replacing stock content with custom creations.

A mod for Re-Volt that aims to replace all original game assets with community made replacements.
Requires RVGL >= 15.1220a.

Check the [Wiki](https://github.com/Yethiel/re-make/wiki) for guidelines and documentation.

[Download the current version](https://github.com/Yethiel/re-make/archive/master.zip)

**License**: Personal use only. Copyright RVCP 2017 ([re-volt.io](http://re-volt.io))

# Changelog

## 18-04-18
+ Cars
   + Adeon Skins "Red Heat" and "Storm Blizzard" by Centroid the Madman

## 17-06-23
+ Cars
    + AMW (by Mladen)

## 17-06-22
+ Cars
    + Adeon (by Mladen)

## 17-06-20
+ Textures
	+ nhood1d and nhood1f.bmp (by Gamer121)

## 17-06-18
+ Textures
	+ stuntsd.bmp (by Gamer121)
	+ envstill.bmp (by Marv)
	+ clouds*.bmp (by Allan)
	+ markara and markarf.bmp (by Allan)
	+ market1a, market1b and market1f.bmp (by Allan)
	+ market2a, market2b and market2f.bmp (by Allan)
	+ muse2g, muse2i and muse2j.bmp (by Allan)
	+ muse_batg and muse_bati.bmp (by Allan)
	+ dragon and toy2i.bmp (by Allan)
	+ water.bmp (toylite) (by Allan)
	+ frontenda and frontendc.bmp (by Marv)
+ Sounds (by Allan)
	+ balldrop.wav
	+ bottle.wav
	+ scrape.wav
	+ garden
		+ animal2.wav
		+ stream.wav
		+ tropics2.wav
		+ tropics4.wav
	+ ghost
		+tumbweed.wav
	+ hood
		+ birds2.wav
		+ dogbark.wav
		+ stram.wav
		+ tv.wav
	+ market
		+ cabnhum2.wav
		+ carton.wav
		+ freezer1.wav
	+ muse
		+ alarm2.wav
	+ toy
		+ arcade.wav
		+ copter.wav
		+ creak.wav
		+ piano.wav
		+ plane.wav
+ Models (most of them by Allan)
	+ edit mode models (by Marv)
	+ abcblock.m
	+ boat1.m
	+ boat2.m
	+ bottle.m
	+ Bottle.hul
	+ bucket.hul
	+ bucket.m
	+ copter.m
	+ copter2.m
	+ copter3.m
	+ earth.m
	+ football.m
	+ horse.m
	+ jupiter.m
	+ mars.m
	+ mercury.m
	+ moon.m
	+ n2trafficcone.hul
	+ n2trafficcone.m
	+ neptune.m
	+ plane.m
	+ plane2.m
	+ pluto.m
	+ radar.m
	+ rings.m
	+ saturn.m
	+ speedup.m
	+ trafficcone.hul
	+ trafficcone.m
	+ uranus.m
	+ venus.m

## 17-03-26
+ Volken Turbo (textures with Gotolei's remapped PRMs)
+ improved German string file
+ userdoc for RVGL (based on http://rv12.revoltzone.net/downloads/rv1.2_userdoc.txt)
